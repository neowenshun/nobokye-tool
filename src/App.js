import './App.css';
import { BrowserRouter } from 'react-router-dom'
import BaseRouter from './Router/routes';
import {NobokyeSystem} from 'nobokye-feedback-tool'

function App() {

  return (
    <div className="App">
      <BrowserRouter>
        <BaseRouter />
      </BrowserRouter>
    </div>
  );
}

export default App;
