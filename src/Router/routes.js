import React from "react";
import { Route, Switch } from "react-router-dom";
import page1 from "../containers/page1";
import page2 from "../containers/page2";

const BaseRouter = () => {
    return (
        <Switch>
            <Route exact path='/' exact component={page1} />
            <Route exact path='/page2' exact component={page2} />
        </Switch>
    )
};

export default BaseRouter;