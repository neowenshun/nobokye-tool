import React from "react";
import {Link} from "react-router-dom" 

export default class page2 extends React.Component {
    render(){
        return (
            <header className="App-header" style={{ marginBottom: 1500 }}>
                <h1>Test Page 2</h1>
                <h2>We are on page 2</h2>
                <div>
                <Link to="/"><button>Go to page 1</button></Link>
                    <button onClick={() => alert('Event fired')} style={{ backgroundColor: "grey" }}>Test mouse event</button>
                </div>
            </header>
        )
    }
}