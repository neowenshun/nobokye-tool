import React, { Fragment } from "react";
import { Link } from 'react-router-dom';

export default class page1 extends React.Component {
    render() {
        return (
            <Fragment>
                <header className="App-header" style={{ paddingBottom: 1500 }}>
                    <h1>Test Page 1</h1>
                    <h2>We are on page 1</h2>
                    <div>
                        <Link to="/page2/"><button>Go to page 2</button></Link>
                        <button onClick={()=>alert('Event fired')} style={{ backgroundColor: "grey" }}>Test mouse event</button>
                    </div>
                </header>
            </Fragment>
        );
    }
}